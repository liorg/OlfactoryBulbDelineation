# Olfactory Bulb (OB) Delineation

This software accelerates the process of demarcating olfactory bulbs by automatically finding the area of the bulbs, and then presenting all the relevant slices simultaneously on-screen, together with customized drawing and zooming tools.

The actual demarcation remains manual.


## Running

To run OB demarcation software:
- Open Matlab
- Run the file “initOlfactoryBulbMarksGUI.m”.
- The following GUI should be open:

![Main GUI](/Images/Main screen.png)
- Press "Load case" button and select any NIfTI file.
- Using the displayed GUI, rotate the images until aligned coronal slice will be displayed.

![Aligned coronal slice](/Images/Aligned coronal slice.png)
- User may add marks of the OB limits
  - User may mark in two different colors (using left and right buttons of the mouse)
  - User may remove exist mark (using the middle button of the mouse)
  - User may change the brightness of the image (using gestures over the background of the image)
  - After each mark change, the volume (in voxels) for each color, and area (in pixels) within each slice, are printed to the Matlab's console.
 
To change the default value(s):
- Edit the file “LoadParametersForImagesDisplayForMark.m”

## Contirbutors
- Date: July 16, 2019
- Version: 1.0
- Author: Lior Gorodisky, Noam Sobel's Olfaction Lab, Weizmann Institute of Science
- Contact: lior.gorodisky@weizmann.ac.il

If you find this code useful please credit us in any papers/software published

 
## Algorithm description

### Step 1 - Find the eyes
- We find the edges of the objects in all coronal slices using canny algorithm.
- From those edges, we find circles with radii of 6-14 millimeters using Hough algorithm, and only those that contain less than 5% edge-pixels in them are considered as optional circles for the eyes.
- Those circles are then filtered by finding the median y-coordinate of the centers, and filtering out circles with y-coordinate, which is more than 2-times of the radius from the median y-coordinate.

### Step 2 - Define bulbs initial box-volume
- We selected the coronal slices of the initial box as the slices matched for 28 millimeters, starting from 6 millimeters before the last slice with matched circle.
- We selected the height of the rows of this box to be 24 millimeters. We found for each circle the row that is 1.25 times the radius above the circle center. Then, we define the average of all those rows (one for each eye-circle) as the upper row of the box.
- We selected the width of the columns of this box to be 22 millimeters. We split all the eye-circles to left-circles and right-circles, and then find the average x-coordinate for each eye. Then, we defined the x-center of the initial box-volume as the center between the x-coordinates of the two eyes.

### Step 3 - Interpolate the bulbs box-volume
- We interpolate each coronal slice using cubic interpolation, with 32-times higher resolution in each dimension.

### Step 4 - Decimate bulbs box-volume width by finding CSF
- We defined a threshold intensity as 16% of the max-intensity through all brain scans, so WM and air are below the threshold, while the GM and CSF are above the threshold, defined as “GM and CSF threshold”
- From all the voxels within the interpolated box-volume that are above the GM and CSF threshold (as defined above), we define the CSF threshold as the top 5% of the intensities-values, defined as “Initial CSF voxels”
- Within each slice, we split the initial CSF voxels into 2D connected-components, and removed areas that failed to pass one of pre-defined criteria (smaller than 22 millimeters squared, width of 9 millimeters at most, x-center between 20% to 80% of the box-width). The passed voxels defined as “CSF voxels”.
- We dilate each CSF area by one imaging-voxel (which are 32-voxels, due to the interpolation), and mark each dilated CSF voxel that has symmetric dilated CSF voxel around the x-center of the box-volume as “Symmetric CSF voxel”
- We split all the CSF voxels into 3D connected-components, and removed volumes that has less than 33% of the voxels marked as “Symmetric CSF voxel” or that are smaller than 10 millimeters cubed.
- We dilate each CSF area by 1.5 imaging-voxel (which are 48-voxels, due to the interpolation), and split them into 3D connected-components, defined as “Bulb CSF candidate”. We then removed 3D Volumes with depth of less than 11 millimeters.
- From the filtered 3D volumes, we defined the five 3D-volumes with highest areas as “Bulb CSF area”.
- We selected the columns of this decimated box to fit to the columns required for the “Bulb CSF area”.

