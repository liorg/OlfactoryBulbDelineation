function [concatenatedImages, concatenatedCoordinates] = ConcatenateImages(images, rowsInConcatenated, coordinates)
% coordinatesToConcatenate in the format <x, y, img-index>

if nargin == 2
    coordinates = [];
end

imagesSizes = size(images);
numOfDimensions = numel(imagesSizes);
if numOfDimensions > 2
    numOfImages = imagesSizes(end);
else
    numOfImages = 1;
end

colsInConcatenated = ceil(numOfImages / rowsInConcatenated);

switch numOfDimensions
    case {2, 3}
        thirdDimensionsInOutput = 1;
    case 4
        thirdDimensionsInOutput = imagesSizes(3);
    otherwise
        error('Not supported number of dimensions: %d', numOfDimensions);
end
minValues = min(images(:));
concatenatedImages = minValues * ones(imagesSizes(1) * rowsInConcatenated, ...
    imagesSizes(2) * colsInConcatenated, thirdDimensionsInOutput);
concatenatedCoordinates = zeros(size(coordinates, 1), ...
    size(coordinates, 2) - 1);

for imgIndex = 1:numOfImages
    colIndex = mod(imgIndex-1, colsInConcatenated);
    rowIndex = floor((imgIndex-1) / colsInConcatenated);
    
    currentRows = imagesSizes(1) * rowIndex + (1:imagesSizes(1));
    currentCols = imagesSizes(2) * colIndex + (1:imagesSizes(2));
    
    switch numOfDimensions
        case {2, 3}
            concatenatedImages(currentRows, currentCols) = images(:,:, imgIndex);
        case 4
            concatenatedImages(currentRows, currentCols, :) = images(:,:,:, imgIndex);
        otherwise
            error('Not supported number of dimensions: %d', numOfDimensions);
    end
    
    if ~isempty(coordinates)
        relevantCoordinatesBool = coordinates(:,end) == imgIndex;
        concatenatedCoordinates(relevantCoordinatesBool, 1) = coordinates(relevantCoordinatesBool, 1) + imagesSizes(2) * colIndex;
        concatenatedCoordinates(relevantCoordinatesBool, 2) = coordinates(relevantCoordinatesBool, 2) + imagesSizes(1) * rowIndex;
    end
end
end