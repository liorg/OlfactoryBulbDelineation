function fig = DisplayImagesForMark(caseName, dicImgSortedClear, voxelDimensions, parametersToUse, dataFolderIndex, waitbarHandle)

if ~exist('dataFolderIndex', 'var')
    dataFolderIndexStr = 'nan';
else
    dataFolderIndexStr = num2str(dataFolderIndex);
end

if ~exist('waitbarHandle', 'var')
    waitbarHandle = [];
end

%% Constants and thresholds
thresholdForPrctile = max(dicImgSortedClear(:)) / 6;
circlesRadiiRangeInMilimeters = [5.8, 13.7];
circlesRadiiRangeInPixels = round(circlesRadiiRangeInMilimeters./voxelDimensions(1:2));
precentOfAllowedEdgesInsideCircle = 5; % 5% of inside-area of circle may be edges
precentOfMinimalPerimeterPixels = 20; % 20% of perimeter pixels of circle must be edges

firstBulbSliceOffsetInMilimeters = GetValue(parametersToUse, 'FirstBulbSliceOffsetInMilimeters', 6);
slicesDepthToDisplayInMilimeters = GetValue(parametersToUse, 'SlicesDepthToDisplayInMilimeters', 28);

widthToDisplayInMilimeters = GetValue(parametersToUse, 'WidthToDisplayInMilimeters', 22);
heightToDisplayInMilimeters = GetValue(parametersToUse, 'HeightToDisplayInMilimeters', 24);

interpolationMult = GetValue(parametersToUse, 'InterpolationMult', 16); %[4, 8, 10, 16];
requiredRatioBetweenWidthAndHeight = GetValue(parametersToUse, 'RequiredRatioBetweenWidthAndHeight', 2);

% bulbMaxSlicesDepth = 30;
maximalWidthOfRegionInMilimeters = 9;
maximalAreaOfRegionInMilimetersSquare = 22;
minimalVolumeInMilimetersCubed = 3;
minimalConnectedVolumeSlicesLengthInMilimeters = 11;
regionHaveBothSideVoxelThresholdVolumeInMilimetersCubed = 10;
coloredWidthRequiredInMilimeters = 20;
diffFromFirstBulbRowToEyeCenterInEyeRadiusUnits = 1.25;

%% Calculate hough for each image, and save circles with less then 5% of the inside-area marked as edges (to find the area of the eyes)
UpdateWaitbar(waitbarHandle, 0.3, 'Finding circles in all slices...');
foundCircles = FindCirclesWithoutInsideEdges(dicImgSortedClear, ...
    circlesRadiiRangeInPixels, precentOfAllowedEdgesInsideCircle, ...
    precentOfMinimalPerimeterPixels, ...
    waitbarHandle);

%% Remove circles far away from the median-y
allRadii = [foundCircles.Radius];
allCenterY = [foundCircles.CenterY];

medianOfCenterY = median(allCenterY);
relevantCenterY = abs(allCenterY - medianOfCenterY) <= 2*allRadii;
foundCirclesRelevant = foundCircles(relevantCenterY);

%% Split the centers between left and right eye
UpdateWaitbar(waitbarHandle, 0.5, 'Splitting circles between eyes...');
[leftEyeCircles, rightEyeCircles] = SplitCentersBetweenEyes(foundCirclesRelevant);
bothEyesCircles = [leftEyeCircles; rightEyeCircles];

%% Decide if to flip the order of the slices
eyesCirclesImages = unique([bothEyesCircles.ImageIndex]);
numberOfSlices = size(dicImgSortedClear, 3);
if median(eyesCirclesImages) > numberOfSlices / 2 % the eyes are at the "later" half - should change order
    dicImgSortedClear = dicImgSortedClear(:, :, end:-1:1);
    for circleIndex = 1:numel(bothEyesCircles)
        bothEyesCircles(circleIndex).ImageIndex = numberOfSlices-bothEyesCircles(circleIndex).ImageIndex;
    end
    fprintf('Note that slices ordered was flipped...\n');
end    

%% display only relevant area (120% of the radius of circles) of the images, in one big image
UpdateWaitbar(waitbarHandle, 0.55, 'Creating eyes image...');
DisplayAndSaveEyesArea(bothEyesCircles, dicImgSortedClear, requiredRatioBetweenWidthAndHeight, ['Eyes-', caseName]);
close;


%% Choose slices for bulb - 5 from last-eye, for 20 forward
lastEyeSlice = max([bothEyesCircles.ImageIndex]);
firstBulbSliceOffsetInSlices = firstBulbSliceOffsetInMilimeters / voxelDimensions(3);
firstBulbSlice = max(1, lastEyeSlice - ceil(firstBulbSliceOffsetInSlices));
% bulbMaxSlicesToUse = ceil(bulbMaxSlicesDepth / voxelDimensions(3));
% lastBulbSlice = min(firstBulbSlice + bulbMaxSlicesToUse, size(dicImgSortedClear, 3));
firstBulbSliceToDisplay = max(1, lastEyeSlice - ceil(1.5*firstBulbSliceOffsetInSlices));
numberOfSlicesToDisplay = ceil(slicesDepthToDisplayInMilimeters / voxelDimensions(3));
lastBulbSliceToDisplay = firstBulbSlice + numberOfSlicesToDisplay; % always display 33 milimeters of slices

%% Find first and last column to display, as fixed width of pixels
middleBetweenEyesX = 0.5 * (mean([leftEyeCircles.CenterX]) + mean([rightEyeCircles.CenterX]));
widthToDisplayInPixels = ceil(widthToDisplayInMilimeters / voxelDimensions(2)); % pixels
firstBulbColumnToDisplay = round(middleBetweenEyesX - widthToDisplayInPixels/2);
lastBulbColumnToDisplay = round(middleBetweenEyesX + widthToDisplayInPixels/2);

%% Find first and last column to use, as the limit columns with 50% blank in the first bulb-slice (in the eye diameter)
UpdateWaitbar(waitbarHandle, 0.6, 'Finding bulbs widths...');
% [firstBulbColumn, lastBulbColumn] = FindBulbColumns(dicImgSortedClear(:,:,firstBulbSlice), leftEyeCircles, rightEyeCircles);

%% Find first and last rows to use, from the center of the eye to the upper limit
eyesCentersY = [bothEyesCircles.CenterY];
eyesRadii = [bothEyesCircles.Radius];
% firstBulbRow = floor(min(eyesCentersY - eyesRadii));
% lastBulbRow = ceil(max(eyesCentersY + 0.25 * eyesRadii));

%% Find first and last rows to display, as fixed height of pixels
heightToDisplayInPixels = heightToDisplayInMilimeters/voxelDimensions(1); % pixels
firstBulbRowToDisplay = round(mean(eyesCentersY - diffFromFirstBulbRowToEyeCenterInEyeRadiusUnits*eyesRadii));
lastBulbRowToDisplay = firstBulbRowToDisplay + heightToDisplayInPixels;

%% Display bulb area in all slices
UpdateWaitbar(waitbarHandle, 0.65, 'Creating bulbs non-interpolated image...');
DisplayAndSaveBulbsArea(dicImgSortedClear, firstBulbSlice, lastBulbSliceToDisplay, ...
    firstBulbColumnToDisplay, lastBulbColumnToDisplay, firstBulbRowToDisplay, lastBulbRowToDisplay, ...
    requiredRatioBetweenWidthAndHeight, ...
    ['Bulbs-', caseName]);
close;

%% Interpolate bulbs in different sizes and methods
relevantRows = firstBulbRowToDisplay:lastBulbRowToDisplay;
relevantCols = firstBulbColumnToDisplay:lastBulbColumnToDisplay;
relevantSlices = firstBulbSliceToDisplay:min(lastBulbSliceToDisplay, size(dicImgSortedClear, 3));
numOfRelevantSlices = numel(relevantSlices);
relvantBulbImages = dicImgSortedClear(relevantRows, relevantCols, relevantSlices);

interpolationMethods = {'cubic'}; %{'linear', 'spline', 'cubic'};

UpdateWaitbar(waitbarHandle, 0.7, 'Interpolating bulbs area...');
for interpolationMultIndex = 1:numel(interpolationMult)
    for interpolationMethodIndex = 1:numel(interpolationMethods)
        maximalWidthOfRegion = maximalWidthOfRegionInMilimeters/voxelDimensions(2)*interpolationMult(interpolationMultIndex);
        maximalAreaOfRegion = maximalAreaOfRegionInMilimetersSquare / prod(voxelDimensions(1:2))*interpolationMult(interpolationMultIndex)^2;
        minimalVolumeInVoxels = minimalVolumeInMilimetersCubed / prod(voxelDimensions)*interpolationMult(interpolationMultIndex)^2;
        minimalNumberOfSlicesInConnectedVolume = minimalConnectedVolumeSlicesLengthInMilimeters/voxelDimensions(3);
        regionHaveBothSideVoxelThresholdVolume = regionHaveBothSideVoxelThresholdVolumeInMilimetersCubed / prod(voxelDimensions)*interpolationMult(interpolationMultIndex)^2; % more than given number of voxels is in both sides
        coloredWidthRequired = coloredWidthRequiredInMilimeters/voxelDimensions(2)*interpolationMult(interpolationMultIndex);
        
        interpolationStep = 1/interpolationMult(interpolationMultIndex);
        interpolationMethod = interpolationMethods{interpolationMethodIndex};
        
        [interpolationBaseX, interpolationBaseY] = meshgrid(1:size(relvantBulbImages,2), 1:size(relvantBulbImages, 1));
        [interpolationDestX, interpolationDestY] = meshgrid(1:interpolationStep:size(relvantBulbImages,2), 1:interpolationStep:size(relvantBulbImages,1));
        
        interpolationResultPerSlice = cell(numOfRelevantSlices, 1);
        interpolationResultPerSliceEdges = cell(numOfRelevantSlices, 1);
        for sliceIndex = 1:numOfRelevantSlices
            UpdateWaitbar(waitbarHandle, 0.7+0.1*sliceIndex/numOfRelevantSlices, ['Interpolating bulbs area (#' num2str(sliceIndex) ' out of ' num2str(numOfRelevantSlices) ')...']);
            interpolationResultPerSlice{sliceIndex} = interp2(interpolationBaseX, interpolationBaseY, ...
                relvantBulbImages(:,:,sliceIndex), ...
                interpolationDestX, interpolationDestY, ...
                interpolationMethod);
            interpolationResultCurrentSlice = interpolationResultPerSlice{sliceIndex};
            interpolationResultCurrentSlice(interpolationResultCurrentSlice <= thresholdForPrctile) = thresholdForPrctile;
            interpolationResultPerSliceEdges{sliceIndex} = edge(interpolationResultCurrentSlice);
        end
        interpolationResult = cat(3, interpolationResultPerSlice{:});
%         interpolationResultEdges = cat(3, interpolationResultPerSliceEdges{:});
        
        interpolationResultValues = interpolationResult(:);
        y = prctile(interpolationResultValues(interpolationResultValues>=thresholdForPrctile), 0:100);
        interpolationResultAboveThreshold = interpolationResult > y(95);
        
        centroid_x_accepted = [0.2, 0.8]*size(interpolationResultAboveThreshold, 2);
        regionHaveBothSideVoxelThresholdRatio = 1/3; % more than 33% of the dilated area is in both sides
        
        UpdateWaitbar(waitbarHandle, 0.8, 'Finding matched regions in bulbs areas...');
        interpolationResultRelevant = interpolationResultAboveThreshold;
        for slice = 1:size(interpolationResultAboveThreshold, 3)
            interpolationResultRelevantSlice = interpolationResultAboveThreshold(:, :, slice);
            connecte2d_props = regionprops(interpolationResultRelevantSlice, {'BoundingBox', 'Area', 'PixelIdxList', 'Centroid'});
            regionsToRemoveIndices = arrayfun(@(props) (props.BoundingBox(3) > maximalWidthOfRegion || props.Area > maximalAreaOfRegion || props.Centroid(1) > centroid_x_accepted(2) || props.Centroid(1) < centroid_x_accepted(1)), connecte2d_props);
            regionsToRemove = connecte2d_props(regionsToRemoveIndices);
            indicesToRemove = cat(1, regionsToRemove.PixelIdxList);
            interpolationResultRelevantSlice(indicesToRemove) = false;
            interpolationResultRelevant(:, :, slice) = interpolationResultRelevantSlice;
        end
        
        interpolationResultRelevantDilated = imdilate(interpolationResultRelevant, strel('square', interpolationMult(interpolationMultIndex)*2));
        interpolationResultRelevantOnlyOneSide = and(interpolationResultRelevant, xor(interpolationResultRelevantDilated, interpolationResultRelevantDilated(:, end:-1:1, :)));
        interpolationResultRelevantOnlyBothSides = xor(interpolationResultRelevant, interpolationResultRelevantOnlyOneSide);
        
        connected3d_botsides_voxels = bwconncomp(interpolationResultRelevantOnlyBothSides);
        connected3d_bothSides_voxelsIndices = cat(1, connected3d_botsides_voxels.PixelIdxList{:});
        
        connected3d = bwconncomp(interpolationResultRelevant);
        pixelIndicesListsInConnected3d = connected3d.PixelIdxList;
        regionHaveBothSideNumOfVoxels = cellfun(@(indicesList) sum(ismember(indicesList, connected3d_bothSides_voxelsIndices)), pixelIndicesListsInConnected3d);
        regionsVolumesInVoxels = cellfun(@(indicesList) numel(indicesList), pixelIndicesListsInConnected3d);
        regionHaveBothSideVoxel = regionHaveBothSideNumOfVoxels ./ regionsVolumesInVoxels;
        relevantRegions = (regionHaveBothSideVoxel > regionHaveBothSideVoxelThresholdRatio) | (regionsVolumesInVoxels > regionHaveBothSideVoxelThresholdVolume);
        pixelIndicesListsInConnected3dWithBothSideVoxels = pixelIndicesListsInConnected3d(relevantRegions);
        
        interpolationResultRelevantAreasWithMirroredVoxels = false(size(interpolationResultRelevantOnlyBothSides));
        interpolationResultRelevantAreasWithMirroredVoxels(cat(1, pixelIndicesListsInConnected3dWithBothSideVoxels{:})) = true;
        
        interpolationResultRelevantAreasWithMirroredVoxelsDilated = imdilate(interpolationResultRelevantAreasWithMirroredVoxels, strel('square', interpolationMult(interpolationMultIndex)*1.5));
        connected3d_bothsides_regions = bwconncomp(interpolationResultRelevantAreasWithMirroredVoxelsDilated);
        pixelIndicesListsInConnected3dWithBothSideVoxelsDilated = connected3d_bothsides_regions.PixelIdxList;
        
        imageSizeInSlice = prod(connected3d.ImageSize(1:2));
        slicesInEachConnectedComponent = cellfun(@(indicesList) numel(unique(ceil(indicesList / imageSizeInSlice))), pixelIndicesListsInConnected3dWithBothSideVoxelsDilated);
        pixelIndicesListsInConnected3dRelevant = pixelIndicesListsInConnected3dWithBothSideVoxelsDilated(slicesInEachConnectedComponent >= minimalNumberOfSlicesInConnectedVolume);
        
        UpdateWaitbar(waitbarHandle, 0.85, 'Finding symmetric areas in bulbs areas...');
        areas = cellfun(@(list) numel(list), pixelIndicesListsInConnected3dRelevant);
        [areasSorted, sortingOrder] = sort(areas, 'descend');
        areasWithMinValues = areasSorted > minimalVolumeInVoxels;
        areasWithMinValues(6:end) = false; % take only the first 5
        relevantAreas = find(areasWithMinValues);
        colored = zeros(size(interpolationResultRelevant));
        for i=1:numel(relevantAreas)
            colored(pixelIndicesListsInConnected3dRelevant{sortingOrder(relevantAreas(i))}) = 10 + numel(relevantAreas) - i;
        end
        colored(~interpolationResultRelevantAreasWithMirroredVoxels) = 0;
        
        relevantColumns = find(sum(sum(colored, 3), 1) > 0);
        if isempty(relevantColumns)
            columnsRangeCenter = round(size(colored, 2)/2);
        else
            columnsRangeCenter = round(mean([relevantColumns(1), relevantColumns(end)]));
        end
        columnsRangeCenter = max(columnsRangeCenter, floor(coloredWidthRequired/2+1));
        columnsRangeCenter = min(columnsRangeCenter, size(colored, 2) - ceil(coloredWidthRequired/2));
        columnsRange = round(columnsRangeCenter + [-0.5, 0.5] * coloredWidthRequired);
        if columnsRange(1) < 1
            columnsRange = columnsRange - columnsRange(1) + 1;
        end
        if columnsRange(2) > size(interpolationResult, 2)
            columnsRange = columnsRange - columnsRange(2) + size(interpolationResult, 2);
            if columnsRange(1) < 1
                columnsRange(1) = 1;
            end
        end
        
        
        %%
        interpolationResultRelvantColumns = interpolationResult(:, columnsRange(1):columnsRange(2), :);
        UpdateWaitbar(waitbarHandle, 0.95, 'Creating bulbs interpolated-image...');
        DisplayAndSaveBulbsArea(interpolationResultRelvantColumns, 1, lastBulbSliceToDisplay - firstBulbSliceToDisplay + 1, ...
            1, size(interpolationResultRelvantColumns, 2), 1, size(interpolationResultRelvantColumns, 1), ...
            requiredRatioBetweenWidthAndHeight, ...
            ['Bulbs-' interpolationMethod '-' num2str(1/interpolationStep) ' - ' dataFolderIndexStr ' - Slice start from code-' num2str(firstBulbSlice)]);
        
        fig = gcf;
        fig.UserData.InterpolationMult = interpolationMult(interpolationMultIndex);
        fig.UserData.OriginalDims = size(interpolationResultRelvantColumns);
        fig.UserData.SubjectName = caseName;
        fig.UserData.MatFilename = [datestr(now, 'yyyy-mm-dd HH_MM_ss') ' - Case ' dataFolderIndexStr '.mat'];
        fig.UserData.FirstBulbSliceOffsetInMilimeters = firstBulbSliceOffsetInMilimeters;
        fig.UserData.SlicesDepthToDisplayInMilimeters = slicesDepthToDisplayInMilimeters;
        userDraw(fig);
    end
end
end