classdef GUIState<handle
    % GUI state object - holds the current state of the GUI
    
    properties(Access = private)
        panelsIDs % a map which its keys are names of the panels and its values are panel's IDs
        toDisplayMarks
        parametersForImagesDisplaying
    end
    
    methods (Access = private)
        % constructor
        function obj = GUIState()
            obj.panelsIDs = containers.Map();
        end
    end % constructor method (private - Singleton)
    
    methods (Static)
        % get an instace of the singleton
        function singleObj = getInstance
            persistent localObj
            if isempty(localObj) || ~isvalid(localObj)
                localObj = GUIState();
            end
            singleObj = localObj;
        end
        
    end
    
    methods(Access = {?OlfactoryBulbMarksGUIManager})
        function SetPanelsIDs(obj, panelName, ids)
            obj.panelsIDs(panelName) = ids;
        end
        
        function SetSDisplayingMarksOption(obj, toDisplayMarks)
            Utils.PrintToLog('[State] Displaying marks is set to: %d\n', toDisplayMarks);
            obj.toDisplayMarks = toDisplayMarks;
        end
        
        function SetAllParametersForImagesDisplaying(obj, parametersStruct)
            Utils.PrintToLog('[State] Parameters struct initalized.\n');
            obj.parametersForImagesDisplaying = parametersStruct;
        end
        
        function SetParameterForImagesDisplaying(obj, parameterName, parameterValue)
            Utils.PrintToLog('[State] Parameter "%s" value changed to: %s\n', parameterName, num2str(parameterValue));
            obj.parametersForImagesDisplaying.(parameterName) = parameterValue;
        end
    end % OlfactoryBulbMarksGUIManager methods
    
    % Public methods
    methods
        function ids = GetPanelsIDs(obj, panelName)
            if exist('panelName', 'var')
                validKeys = isKey(obj.panelsIDs,panelName);
                ids = cell2mat(values(obj.panelsIDs,panelName(validKeys)));
            else
                ids = cell2mat(values(obj.panelsIDs));
            end
        end
        
        function toDisplayMarks = GetWhetherToDisplayMarks(obj)
            toDisplayMarks = obj.toDisplayMarks;
        end
        
        function parametersStruct = GetAllParametersForImagesDisplaying(obj)
            parametersStruct = obj.parametersForImagesDisplaying;
        end
    end
end

