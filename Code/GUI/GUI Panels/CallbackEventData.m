classdef (ConstructOnLoad) CallbackEventData < event.EventData
   properties
      uiHandle = 0;
   end
   methods
      function eventData = CallbackEventData(value)
            eventData.uiHandle = value;
      end
   end
end

