classdef PanelFactory < handle
    % This class is a panel Factory pattern
    
    properties(GetAccess  = private)
        dictionnary = containers.Map(); % Hash table
    end
    
    methods
        function [obj] = PanelFactory()
            % Creates the factory

            % Pre-register some well-known panels
            obj.RegisterPanel(GUIDefinition.buttonsPanelKey, @() ButtonsPanelController);
        end
        
        
        function [] = RegisterPanel(obj, panelName, createSensorCallback)
            % Adds new sensor to the factory
            obj.dictionnary(panelName) = createSensorCallback;
        end
        function [panelsList] = GetListOfPanels(obj)
            % Obtains the list of available panels
            panelsList = obj.dictionnary.keys;
        end
        
        function [panel] = CreatePanel(obj, panelKey, parentHandle, panelName, panelsMngr, panelPos)
            % Creates panel instance
            if nargin < 6
                panelPos = [];
            end
            createCallback = obj.dictionnary(panelKey);
            panel = createCallback();
            panel.InitialPanel(obj.PanelIDDealer(), parentHandle, panelName, panelsMngr, panelPos);
        end
    end
    
    methods(Static = true)
       function panelID = PanelIDDealer()
            persistent pid
            if isempty(pid)
               pid = 0; 
            end
            pid = pid + 1;
            panelID = pid;
        end 
    end
end

