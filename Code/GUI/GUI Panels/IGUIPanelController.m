classdef IGUIPanelController<handle
    %Interface class of GUI panel controller object
    
    properties
        panelID
        panelView
        listeningList
    end
    
    methods(Abstract =true)
        InitialPanel(obj, panelID, parentHandle, panelPos, panelsMngr, panelName);
    end
    
    methods
        function DeletePanel(obj, src, eventdata)
            if any(eventdata.PanelId == obj.panelID)
               obj.RemoveListeners();
               delete(obj);
            end
        end
        
        function RemoveListeners(obj)
            for lidx = 1:numel(obj.listeningList)
                delete(obj.listeningList{lidx});
            end
        end
    end
end

