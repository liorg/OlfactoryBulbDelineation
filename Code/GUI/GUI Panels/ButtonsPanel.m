classdef ButtonsPanel<IGUIPanel
    % Buttons panel object
    
    properties (Constant = true)
        loadCaseButtonPos = [0.03 0.6 0.3 0.13];
        loadMarksButtonPos = [0.03 0.45 0.3 0.13];
        displayMarksCheckBoxPos = [0.03 0.3 0.3 0.13];
        
        textBoxesArea = [0.35 0.05 0.62 0.9];
        textBoxValuePosParameters = [0.02, 0, 0.07, 0.06]; % [distance from text-box, N/A, max-width, max-height]
    end
    
    properties
        loadCaseButton
        loadMarksButton
        displayMarksCheckBox
    end
    
    events
       loadCaseEvent
       loadMarksEvent
       changeWhetherToDisplayMarksEvent
       changeParameterValueEvent
    end
    
    methods

        function InitialPanel(obj, panelID, parentHandle, panelPos, panelsMngr, panelName) %#ok<INUSD>
            obj.panelID = panelID;
            obj.panelsMngr = panelsMngr;
            obj.CreatePanel(parentHandle, panelPos);
            
            guiState = GUIState.getInstance();
            
            % Create buttons
            obj.loadCaseButton = obj.CreateButton(obj.loadCaseButtonPos, 'Load Case', {@obj.PanelNotify,'loadCaseEvent'}, 'LoadCaseButton', 'ForegroundColor', [0, 1, 0], 'FontWeight', 'bold');
            obj.loadMarksButton = obj.CreateButton(obj.loadMarksButtonPos, 'Load Marks', {@obj.PanelNotify,'loadMarksEvent'}, 'LoadMarksButton', 'ForegroundColor', [0, 1, 0], 'FontWeight', 'bold');
            
            % Create check-box
            obj.displayMarksCheckBox = obj.CreateToggle(obj.displayMarksCheckBoxPos, 'DisplayMarksCheckBox', ...
                'Display marks', {@obj.PanelNotify,'changeWhetherToDisplayMarksEvent'});
            
            % Create text-boxes
            parametersForImagesDisplay = guiState.GetAllParametersForImagesDisplaying();
            parametersForImagesDisplayFields = fieldnames(parametersForImagesDisplay);
            numberOfFields = numel(parametersForImagesDisplayFields);
            textBoxesHeight = obj.textBoxesArea(4) / numberOfFields;
            
            for fieldIndex = 1:numberOfFields
                parameterTextBoxPos = [obj.textBoxesArea(1), obj.textBoxesArea(2) + textBoxesHeight * (numberOfFields - fieldIndex), ...
                    obj.textBoxesArea(3), textBoxesHeight];
                
                parameterString = parametersForImagesDisplayFields{fieldIndex};
                parameterValue = parametersForImagesDisplay.(parameterString);
                parameterStringWithSpaces = regexprep(parameterString, '([a-z])([A-Z])', '$1 $2');
                parameterStringClear = [parameterStringWithSpaces(1), lower(parameterStringWithSpaces(2:end))];
                currentTextBox = obj.CreateTextBox(parameterTextBoxPos, [parametersForImagesDisplayFields{fieldIndex} '_TextBox'], parameterStringClear, ...
                    'HorizontalAlignment', 'left');
                
                currentTextBoxHeight = currentTextBox.Extent(4);
                currentTextBoxWidthWithMargin = currentTextBox.Extent(3) + obj.textBoxValuePosParameters(1);
                parameterValuePos = parameterTextBoxPos + [currentTextBoxWidthWithMargin, 0, -currentTextBoxWidthWithMargin , 0];
                parameterValuePos(3) = min(parameterValuePos(3), obj.textBoxValuePosParameters(3));
                if parameterValuePos(4) > obj.textBoxValuePosParameters(4)
                    heightsDiff = parameterValuePos(4) - currentTextBoxHeight + 0.01;
                    parameterValuePos(4) = obj.textBoxValuePosParameters(4);
                    parameterValuePos(2) = parameterValuePos(2) + heightsDiff;
                end

                obj.CreateEditableTextField(parameterValuePos, [parametersForImagesDisplayFields{fieldIndex} '_Value'], num2str(parameterValue), ...
                    {@obj.PanelNotify,'changeParameterValueEvent'});
            end
            
            obj.AddListeners();
        end
        
        function AddListeners(obj)
            guiManager = obj.panelsMngr;
            obj.listeningList = {};
            obj.listeningList{end+1} = addlistener(guiManager, 'deletePanelEvent', @obj.DeletePanel);
            obj.listeningList{end+1} = addlistener(guiManager, 'enableButtonsEvent', @obj.EnableButtons);
            obj.listeningList{end+1} = addlistener(guiManager, 'disableButtonsEvent', @obj.DisableButtons);
            obj.listeningList{end+1} = addlistener(guiManager, 'forceDisplayingMarksEvent', @obj.ForceDisplayingMarks);
        end
        
        function CreatePanel(obj, parentHandle, panelPos)
            obj.panelHandle =  uipanel('Parent',parentHandle,...
                'Tag',sprintf('ButtonsPanel%d', obj.panelID),...
                'BackgroundColor',GUIDefinition.defaultPanelbackgroundColor,...
                'BorderWidth',1);
            
            if exist('panelPos', 'var') && ~isempty(panelPos)
                % define panel's position
                set(obj.panelHandle, 'Position', panelPos);
            end
        end
        
        function DisableButtons(obj, src, eventData)
            if ~any(eventData.PanelId == obj.panelID)
                return;
            end
            
            DisableButtons@IGUIPanel(obj, src, eventData);
        end
        
        function EnableButtons(obj, src, eventData)
            if ~any(eventData.PanelId == obj.panelID)
                return;
            end
            
            EnableButtons@IGUIPanel(obj, src, eventData);
        end
    end
    
    methods (Access = private)
        function ForceDisplayingMarks(obj, src, eventData) %#ok<INUSD>
            set(obj.displayMarksCheckBox, 'Value', true);
        end
    end
end

