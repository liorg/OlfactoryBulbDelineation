classdef ButtonsPanelController<IGUIPanelController
    %Buttons Panel controller object
    methods
        % constructor
        function obj = ButtonsPanelController()
        end
        
        % initializer
        function InitialPanel(obj, panelID, parentHandle, panelName, panelsMngr, panelPos)
            if nargin < 5
                panelPos = [];
            end
            obj.panelID = panelID;
            obj.panelView = ButtonsPanel();
            obj.panelView.InitialPanel(panelID, parentHandle, panelPos, panelsMngr, panelName);
            obj.AddListeners(obj.panelView);
        end
        
        function AddListeners(obj, src_handle)
            obj.listeningList = {};
            obj.listeningList{end+1} = addlistener(src_handle, 'loadCaseEvent', @obj.LoadCase);
            obj.listeningList{end+1} = addlistener(src_handle, 'loadMarksEvent', @obj.LoadMarks);
            obj.listeningList{end+1} = addlistener(src_handle, 'changeWhetherToDisplayMarksEvent', @obj.ChangeWhetherToDisplayMarks);
            obj.listeningList{end+1} = addlistener(src_handle, 'changeParameterValueEvent', @obj.ChangeParameterValue);
            
            % listening to GUI manager's events
            guiManager = obj.panelView.panelsMngr;
            obj.listeningList{end+1} = addlistener(guiManager, 'deletePanelEvent', @obj.DeletePanel);
        end
        
        % a callback functions of the buttons
        function LoadCase(obj, src, eventData) %#ok<INUSD>
            obj.panelView.panelsMngr.LoadCase();
        end
        
        function LoadMarks(obj, src, eventData) %#ok<INUSD>
            obj.panelView.panelsMngr.LoadMarks();
        end
        
        % a callback function of changing add-practice-event
        function ChangeWhetherToDisplayMarks(obj, src, eventData)
            toDisplayMarks = obj.ExtractCurrentCheckBoxValue(eventData);
            obj.panelView.panelsMngr.WhetherToDisplayMarks(toDisplayMarks);
        end
        
        function ChangeParameterValue(obj, src, eventData)
            changedHandle = eventData.uiHandle;
            changedHandleFieldname = strrep(changedHandle.Tag, '_Value', '');
            changedHandleValue = str2double(changedHandle.String);
            if isnan(changedHandleValue)
                guiState = GUIState.getInstance();
                parametersToUse = guiState.GetAllParametersForImagesDisplaying();
                lastUsedValue = parametersToUse.(changedHandleFieldname);
                changedHandle.String = num2str(lastUsedValue);
                Utils.PrintToLog('New value for field "%s" is not number. Will ignore that and use the previous value: %s.\n', changedHandleFieldname, num2str(lastUsedValue))
                return;
            end
            obj.panelView.panelsMngr.ChangeUsedParameter(changedHandleFieldname, changedHandleValue);
        end
    end
    
    methods (Access = private, Static = true)
        function checkBoxValue = ExtractCurrentCheckBoxValue(eventData)
            checkBoxHandle = eventData.uiHandle;
            checkBoxValue = get(checkBoxHandle, 'Value');
        end
    end
end

