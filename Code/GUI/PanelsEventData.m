classdef (ConstructOnLoad) PanelsEventData < event.EventData
   properties
      PanelId = 0;
      additionalInfo = [];
   end
   methods
      function eventData = PanelsEventData(value, varargin)
            eventData.PanelId = value;
            if nargin > 1
               eventData.additionalInfo = varargin{:}; 
            end
      end
   end
end

