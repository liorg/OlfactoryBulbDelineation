classdef (Sealed) OlfactoryBulbMarksGUIManager<handle
    
    properties (Constant = true, Access = private)
        mainWindowName = 'Olfactory-Bulb marks GUI';
        mainWindowTag = 'MainWindow';
        mainWindowPositon = [0, 0, 620, 400]; % only the width and height is important
    end
    
    properties
        panelFactory
        isDiaryOpened
        
        currentMarksFigure
    end
    
    events
        deletePanelEvent
        enableButtonsEvent
        disableButtonsEvent
        
        forceDisplayingMarksEvent
    end
    
    methods (Static)
        % get an instace of the singleton
        function singleObj = getInstance
            persistent localObj
            if isempty(localObj) || ~isvalid(localObj)
                localObj = OlfactoryBulbMarksGUIManager();
            end
            singleObj = localObj;
        end
        
    end
    
    methods (Access = private)
        % constructor
        function obj = OlfactoryBulbMarksGUIManager
            % initial GUI state
            guiState = GUIState.getInstance();
            % initial panels factory pattern
            obj.panelFactory = PanelFactory();
            obj.isDiaryOpened = false;
            obj.currentMarksFigure = [];
            
            % Initial parameters for image-displaying
            guiState.SetAllParametersForImagesDisplaying(LoadParametersForImagesDisplayForMark());
        end
        
        function ResetPanels(obj, mainWindow)
            obj.ChangePointer('watch')
            % delete old panels
            notify(obj, 'deletePanelEvent', PanelsEventData(GUIState.getInstance.GetPanelsIDs));
            
            % and restart panels
            obj.CreatePanels(mainWindow);
            
            obj.ChangePointer('arrow')
        end
        
        function mainWindow = CreateGuiMainWindow(obj)
            % Create main window
            mainWindow = ...
                figure('ToolBar','figure',...
                'Units','pixels',...
                'WindowStyle','normal',...
                'DeleteFcn',@obj.DeleteObj,...
                'CloseRequestFcn',@obj.DeleteObj,...
                'ToolBar','none',...
                'MenuBar','none',...
                'renderer','opengl',...
                'numbertitle', 'off', ...
                'Tag',obj.mainWindowTag,...
                'Name',obj.mainWindowName,...
                'outerposition', obj.mainWindowPositon);
            
            %% position the main-window on the center of the screen
            currentUnitsOfMainWindow = get(mainWindow, 'Units');
            set(mainWindow, 'Units', 'normalized');
            positionOfMainWindowNormalized = get(mainWindow, 'OuterPosition');
            positionOfMainWindowNormalized(1:2) = (1 - positionOfMainWindowNormalized(3:4))/2;
            set(mainWindow, 'OuterPosition', positionOfMainWindowNormalized);
            set(mainWindow, 'Units', currentUnitsOfMainWindow);
        end
        
        function CreatePanels(obj, fig)
            guiState = GUIState.getInstance;
            
            buttonsPanelHeight = 1;
            
            % create buttons panel
            ipc1Panel = uipanel('Parent',fig ,'Title','', 'BorderType', 'none', 'BorderWidth', 0, 'Position',[0, 1-buttonsPanelHeight, 1, buttonsPanelHeight]);
            ipc1 = obj.panelFactory.CreatePanel(GUIDefinition.buttonsPanelKey, ipc1Panel, 'Buttons', obj);
            guiState.SetPanelsIDs('Buttons', ipc1.panelID);
            
            notify(obj, 'disableButtonsEvent', PanelsEventData(ipc1.panelID));
        end
        
        function DisableButtons(obj)
            notify(obj, 'disableButtonsEvent', PanelsEventData(GUIState.getInstance.GetPanelsIDs()));
            drawnow;
        end
        
        function EnableSpecificPanelButtons(obj, panelID)
            notify(obj, 'enableButtonsEvent', PanelsEventData(panelID));
        end
        
        function StartDiary(obj)
            diaryFilename = '';
            
            if isdeployed
                args = System.Environment.GetCommandLineArgs;
                if numel(args) > 0
                    exefilename = char(args(1));
                    diaryFilename = strrep(exefilename, '.exe', '');
                    diaryFilename = [diaryFilename '.log'];
                end
            end
            
            if isempty(diaryFilename)
                diaryFilename = 'OlfactoryBulbMarks.log';
            end
            
            diary(diaryFilename);
            obj.isDiaryOpened = true;
            
            % Print message
            Utils.PrintToLog('*** New application instance ***\n');
        end
    end
    
    methods
        function Initiate(obj)
            obj.StartDiary();
            Utils.PrintToLog(['[GUI] ' obj.mainWindowName ' is starting.\n']);
            
            mainWindow = obj.CreateGuiMainWindow();
            obj.CreatePanels(mainWindow);
            obj.EnableButtons();
        end
        
        % Close function
        function DeleteObj(obj, varargin)
            % Delete only once
            persistent deleteObj
            if isempty(deleteObj)
                deleteObj = tic;
            elseif toc(deleteObj) < 2
                return;
            end
            
            Utils.PrintToLog(['[GUI] ' obj.mainWindowName ' is  closing.\n']);
            delete(obj.panelFactory);
            
            if obj.isDiaryOpened
                diary off;
            end
            
            delete(gcf);
            delete(GUIState.getInstance);
            delete(obj);
            clear deleteObj;
        end
        
        % change pointer type
        function ChangePointer(obj, pointerMode)
            mainWindow = findobj('Tag',obj.mainWindowTag);
            set(mainWindow,'Pointer',pointerMode);
            drawnow;
        end
        
        function EnableButtons(obj)
            panelsIDs = GUIState.getInstance.GetPanelsIDs({'Buttons'});
            obj.EnableSpecificPanelButtons(panelsIDs);
        end
        
        function LoadCase(obj)
            obj.currentMarksFigure = [];
            notify(obj, 'forceDisplayingMarksEvent', PanelsEventData(GUIState.getInstance.GetPanelsIDs));
            
            supportedCaseExtensions = {'*.nii;*.nii.gz', 'NIfTI files (*.nii, *.nii.gz)'};
            [file, pathToFile] = uigetfile(supportedCaseExtensions, 'Select case file');
            if ~file
                Utils.PrintToLog('[GUI] In LoadCase, the user did not select any file.\n');
                return;
            end
            
            waitbarHandle = waitbar(0.1, 'Loading case...');
            
            niftiFilename = fullfile(pathToFile, file);
            try
                vol = spm_vol(niftiFilename);
            catch
                Utils.DialogWithPrintToLog('Error', 'Failed to run SPM function', 'Loading data failure', 'modal');
                close(waitbarHandle);
                return;
            end
            %% Display the mid-slice and allow the user to choose if to rotate in any direction
            affine_transofmration_parameters = spm_imatrix(vol.mat);
            voxelDimensions = abs(affine_transofmration_parameters(7:9));
            images = vol.dat;
            check_for_rotation = true;
            f = figure;

            while check_for_rotation
                midSliceIndex = ceil(size(images, 3)/2);
                imshow(images(:,:,midSliceIndex), []);
                file_clean = strrep(file, '_', ' ');
                title([file_clean ': Middle slice']);
                
                %% Display question dialog to decide if to rotate or not
                question = 'Should the image will be rotated?';
                questionTitle = 'Image rotation';
                questionButtons = {'No', '90� clock-wise', '180�', '90� counter-clock-wise', 'Change orientation 1', 'Change orientation 2'};
                selectedIndex = listdlg('PromptString', question,...
                    'Name', questionTitle, ...
                    'SelectionMode','single',...
                    'ListSize',[250, 20*numel(questionButtons)], ...
                    'ListString', questionButtons);
                if isempty(selectedIndex)
                    break;
                elseif strcmp(questionButtons{selectedIndex}, 'No')
                    check_for_rotation = false;
                elseif selectedIndex <= 4
                    rotationDeg = -90*(selectedIndex - 1);
                    images = imrotate(images, rotationDeg);
                    if selectedIndex ~= 3
                        voxelDimensions(1:2) = voxelDimensions(2:-1:1);
                    end
                elseif selectedIndex == 5
                    images = permute(images, [3, 2, 1]);
                    voxelDimensions = voxelDimensions(end:-1:1);
                else
                    images = permute(images, [3, 1, 2]);
                    voxelDimensions = voxelDimensions([3, 1, 2]);
                end
            end
            close(f);
            if isempty(selectedIndex)
                Utils.PrintToLog('[GUI] In LoadCase, the user did not select any rotation option.\n');
                close(waitbarHandle);
                return;
            end
            
            %% Display images for mark
            guiState = GUIState.getInstance();
            parametersToUse = guiState.GetAllParametersForImagesDisplaying();
            obj.currentMarksFigure = DisplayImagesForMark(file_clean, images, voxelDimensions, parametersToUse, file_clean, waitbarHandle);
            
            %% Close waitbar
            close(waitbarHandle);
        end
        
        function LoadMarks(obj)
            if isempty(obj.currentMarksFigure)
                Utils.DialogWithPrintToLog('Error', 'You must first load case', 'Loading marks failure', 'modal');
                return;
            end
            notify(obj, 'forceDisplayingMarksEvent', PanelsEventData(GUIState.getInstance.GetPanelsIDs));

            %% Let the user to select the matched .mat file
            supportedCaseExtensions = {'*.mat', 'Matlab data files (*.mat)'};
            [file, pathToFile] = uigetfile(supportedCaseExtensions, 'Select marks file');
            if ~file
                Utils.PrintToLog('[GUI] In LoadMarks, the user did not select any file.\n');
                return;
            end
            
            %% Load lines from file
            allLoaded = load(fullfile(pathToFile, file));
            loadedDataSubjectName = allLoaded.UserData.SubjectName;
            currentCaseLines = allLoaded.LinesData;
            
            %% Verify it's the same subject-name. If not - display warning to user
            currentDisplayedSubjectName = obj.currentMarksFigure.UserData.SubjectName;
            if ~strcmp(loadedDataSubjectName, currentDisplayedSubjectName)
                questionStrings = {['Loaded subject-name (' loadedDataSubjectName ' is different from'], ['current displayed one (' currentDisplayedSubjectName ').'], 'To continue?'};
                responseOptions = {'Yes', 'No'};
                questResponse = questdlg(questionStrings, 'Non matching names', responseOptions{:}, 'Yes');
                if strcmp(questResponse, 'No')
                    Utils.PrintToLog('[GUI] In LoadMarks, user choose not to continue due to mismatch in subject names.\n');
                    return;
                end
            end
            
            %% Remove exist lines
            figureChildren = obj.currentMarksFigure.CurrentAxes.Children;
            nonImagesChildrenIndices = arrayfun(@(child) ~strcmp(child.Type, 'image'), figureChildren);
            nonImagesChildren = figureChildren(nonImagesChildrenIndices);
            delete(nonImagesChildren);
            
            %% Plot all marks with texts
            userData = allLoaded.UserData;
            if isfield(userData, 'InterpolationMult')
                interpolationMult = userData.InterpolationMult;
            else
                interpolationMult = 1;
            end
            figure(obj.currentMarksFigure);
            hold on;
            for lineIndex = 1:numel(currentCaseLines)
                xData = currentCaseLines(lineIndex).Lines_X;
                yData = currentCaseLines(lineIndex).Lines_Y;
                r = plot(xData, yData, ':', 'Color', currentCaseLines(lineIndex).LinesColors, 'LineWidth', 1.5, 'LineStyle', ':');
                r_area = polyarea(xData, yData) / interpolationMult^2;
                mid_x = (min(xData) + max(xData))/2;
                max_y = max(yData);
                r.UserData.TextObj = text(mid_x, max_y + 40, sprintf('%3.1f', r_area), 'Color', r.Color, 'FontSize', 12, 'FontWeight', 'bold');
                r.UserData.TextObj.Position(1) = r.UserData.TextObj.Position(1) - r.UserData.TextObj.Extent(3) / 2;
            end
        end
        
        function WhetherToDisplayMarks(obj, toDisplayMarks)
            if isempty(obj.currentMarksFigure)
                Utils.PrintToLog('[GUI] In WhetherToDisplayMarks, skipping function since current-marks-figure is empty.\n');
                return;
            end
            
            figureChildren = obj.currentMarksFigure.CurrentAxes.Children;
            nonImagesChildrenIndices = arrayfun(@(child) ~strcmp(child.Type, 'image'), figureChildren);
            nonImagesChildren = figureChildren(nonImagesChildrenIndices);
            
            if toDisplayMarks
                [nonImagesChildren.Visible] = deal('on');
            else
                [nonImagesChildren.Visible] = deal('off');
            end
            
        end
        
        function ChangeUsedParameter(obj, fieldname, newValue)
            guiState = GUIState.getInstance();
            guiState.SetParameterForImagesDisplaying(fieldname, newValue);
        end
    end % public methods
end

