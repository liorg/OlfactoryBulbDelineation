classdef GUIDefinition
    
    properties(Constant=true)
        DefaultFontSize = 12;
        defaultPanelbackgroundColor = [0 0 0];
        
        % Panels keys
        buttonsPanelKey = 'ButtonsPanel';
    end
end

