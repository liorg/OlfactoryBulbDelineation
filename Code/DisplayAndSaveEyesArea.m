function DisplayAndSaveEyesArea(foundCirclesRelevant, dicImgSortedClear, requiredRatioBetweenWidthAndHeight, caseName)
allRadii = [foundCirclesRelevant.Radius];
allCenterX = [foundCirclesRelevant.CenterX];
allCenterY = [foundCirclesRelevant.CenterY];

minCircleY = min(allCenterY - allRadii);
maxCircleY = max(allCenterY + allRadii);

areaCirclesMidY = (maxCircleY + minCircleY)/2;
areaCirclesDisY = (maxCircleY - minCircleY)/2;

minX_ToUse = floor(min(allCenterX - allRadii * 1.2));
maxX_ToUse = ceil(max(allCenterX + allRadii * 1.2));
minY_ToUse = floor(areaCirclesMidY - areaCirclesDisY * 1.2);
maxY_ToUse = ceil(areaCirclesMidY + areaCirclesDisY * 1.2);

slicesToAddEachSide = 10;
imagesIndices = [foundCirclesRelevant.ImageIndex];
minImageIndex = max(1, min(imagesIndices)-slicesToAddEachSide);
maxImageIndex = min(size(dicImgSortedClear, 3), max(imagesIndices)+slicesToAddEachSide);

relevantRows = minY_ToUse : maxY_ToUse;
relevantCols = max(1, minX_ToUse):min(maxX_ToUse, size(dicImgSortedClear, 2));
relevantSlices = minImageIndex:maxImageIndex;
relvantImages = dicImgSortedClear(relevantRows, relevantCols, relevantSlices);
numberOfSlices = size(relvantImages, 3);
rowsInConcatenated = floor(sqrt(numberOfSlices / requiredRatioBetweenWidthAndHeight * size(relvantImages,2) / size(relvantImages, 1)));
if rowsInConcatenated == 0
    rowsInConcatenated = 1;
end

circlesInTruncedImages = foundCirclesRelevant;
for circleIndex = 1:numel(foundCirclesRelevant)
    circlesInTruncedImages(circleIndex).CenterY = circlesInTruncedImages(circleIndex).CenterY - (minY_ToUse - 1);
    circlesInTruncedImages(circleIndex).CenterX = circlesInTruncedImages(circleIndex).CenterX - (minX_ToUse - 1);
    circlesInTruncedImages(circleIndex).ImageIndex = circlesInTruncedImages(circleIndex).ImageIndex - (minImageIndex - 1);
end

centersCoordinatesInTruncedImages = [circlesInTruncedImages.CenterX; circlesInTruncedImages.CenterY; circlesInTruncedImages.ImageIndex]';
[relvantImagesConcatenated, centersCoordinatesInConcatenatedImages] = ConcatenateImages(relvantImages, rowsInConcatenated, centersCoordinatesInTruncedImages);
circlesInConcatenatedCell = num2cell([centersCoordinatesInConcatenatedImages, [circlesInTruncedImages.Radius]']);
circlesInConcatenated = cell2struct(circlesInConcatenatedCell, {'CenterX', 'CenterY', 'Radius'}, 2);

fig = figure;
minmaxValues = [min(relvantImagesConcatenated(:)), max(relvantImagesConcatenated(:))];
imshow(relvantImagesConcatenated, minmaxValues);
fig.Units = 'normalized';
fig.Position = [0,0,1,1];
title(caseName);
centers = [circlesInConcatenated.CenterX; circlesInConcatenated.CenterY]';
radii = [circlesInConcatenated.Radius];
viscircles(centers, radii, 'EdgeColor', 'green');

%% Save image
img = getframe(gcf);
imwriteWithCreateFolder(img, ['Images', filesep, caseName, '.bmp']);