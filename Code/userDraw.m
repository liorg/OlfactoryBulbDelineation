function userDraw(fig)

imgHandle = fig.CurrentAxes.Children;
set(imgHandle,'buttondownfcn',@start_pencil)
set(fig,'buttondownfcn',@change_brightness)

function change_brightness(src, eventdata)

coords=get(gca,'currentpoint'); %since this is the axes callback, src=gca
x=coords(1,1,1);
y=coords(1,2,1);
r=line(x, y, 'visible', 'off', 'hittest', 'off'); %turning     hittset off allows you to draw new lines that start on top of an existing line.
% set(gcf,'windowbuttonmotionfcn',{@continue_change_brightness,r})
set(gcf,'windowbuttonupfcn',{@done_change_brightness,r})

function continue_change_brightness(src, eventdata, r)
coords=get(gca,'currentpoint'); %this updates every time i move the mouse
x=coords(1,1,1);
y=coords(1,2,1);
%get the line's existing coordinates and append the new ones.
if ~isvalid(r)
    return;
end
lastx=get(r,'xdata');  
lasty=get(r,'ydata');
dx = x - lastx;
dy = y - lasty;

if dx == 0 && dy == 0 % one click - restore the default color-limits
    currentChildren = get(gca, 'Children');
    currentChildrenImages = currentChildren(strcmp(currentChildren.get('type'), 'image'));
    currentChildrenImageGreyLevels = currentChildrenImages(arrayfun(@(imgObj) size(imgObj.CData, 3) == 1, currentChildrenImages));
    allVoxelsValues = currentChildrenImageGreyLevels.CData(:);
    newColorLim = [min(allVoxelsValues), max(allVoxelsValues)];
    set(gca, 'CLim', newColorLim);
    return;
end

set(r,'xdata',x,'ydata',y);

currentColorLim = get(gca, 'CLim');
currentColorLimCenter = mean(currentColorLim);
currentColorLimWidth = diff(currentColorLim);
newColorLimCenter = currentColorLimCenter + dy/2;
newColorLimWidth = currentColorLimWidth + dx;
if newColorLimWidth < 0
    newColorLimWidth = 0.0001;
end
newColorLim = newColorLimCenter + [-0.5, 0.5]*newColorLimWidth;
set(gca, 'CLim', newColorLim);

function done_change_brightness(src, eventdata, r)
continue_change_brightness(src, eventdata, r);
delete(r)

function start_pencil(src,eventdata)

coords=get(gca,'currentpoint'); %since this is the axes callback, src=gca
x=coords(1,1,1);
y=coords(1,2,1);
switch eventdata.Button
    case 1 % left key
        lineColor = [1, 1, 0];
    case 3 % right key
        lineColor = [1, 0, 0];
    otherwise
        lineColor = [];
end
existAxesChildren = get(gca, 'Children');
existLines = existAxesChildren(arrayfun(@(child) strcmp(child.Type, 'line'), existAxesChildren));
set(existLines, 'Selected', 'off');
pointInPolygons = arrayfun(@(child) inpolygon(x, y, child.XData, child.YData), existLines);
userData = get(gcf, 'UserData');
if isfield(userData, 'InterpolationMult')
    interpolationMult = userData.InterpolationMult;
else
    interpolationMult = 1;
end
distanceFromPointToPolygons = sqrt(arrayfun(@(child) min((x-child.XData).^2 + (y-child.YData).^2), existLines)) / interpolationMult;
[minDistanceFromPointToPolygons, indexOfMinDistance] = min(distanceFromPointToPolygons);
if ~any(pointInPolygons) && numel(distanceFromPointToPolygons) > 0 && minDistanceFromPointToPolygons < 3
    pointInPolygons(indexOfMinDistance) = true;
end
if any(pointInPolygons)
    r = existLines(find(pointInPolygons, 1));
    if eventdata.Button == 2
        if isfield(r.UserData, 'TextObj') && isobject(r.UserData.TextObj)
            delete(r.UserData.TextObj);
        end
        delete(r);
        nested_PrintDataOnAllMarkedAreas(gca);
    else
        if isempty(lineColor)
            error('Unexpected button: %d', eventdata.Button);
        end
        if any(r.Color ~= lineColor)
            r.Color = lineColor;
            r.UserData.TextObj.Color = lineColor;
            nested_PrintDataOnAllMarkedAreas(gca);
        end
        r.UserData.X = x;
        r.UserData.Y = y;
        r.UserData.Button = eventdata.Button;
        set(gcf,'windowbuttonmotionfcn',{@continue_moving,r})
        set(gcf,'windowbuttonupfcn',{@done_moving,r})
    end
else
    if eventdata.Button == 2
        fprintf('Pressed the middle key - you should press the left or right key!\n');
        return;
    end
    if isempty(lineColor)
        error('Unexpected button: %d', eventdata.Button);
    end
    r=line(x, y, 'color', lineColor, 'LineWidth', 1.5, 'LineStyle', ':', 'hittest', 'off'); %turning     hittset off allows you to draw new lines that start on top of an existing line.
    set(gcf,'windowbuttonmotionfcn',{@continue_pencil,r})
    set(gcf,'windowbuttonupfcn',{@done_pencil,r})
end

function continue_pencil(src,eventdata,r)
%Note: src is now the figure handle, not the axes, so we need to use gca.
coords=get(gca,'currentpoint'); %this updates every time i move the mouse
x=coords(1,1,1);
y=coords(1,2,1);
%get the line's existing coordinates and append the new ones.
lastx=get(r,'xdata');  
lasty=get(r,'ydata');
newx=[lastx x];
newy=[lasty y];
set(r,'xdata',newx,'ydata',newy);

function done_pencil(src,evendata,r)
%all this funciton does is turn the motion function off 
set(gcf,'windowbuttonmotionfcn','')
set(gcf,'windowbuttonupfcn','')
if numel(r.XData) <= 5
    if numel(r.XData) > 1
        fprintf('Number of marked pixels is less than 5 - will remove it.\n');
    end
    delete(r);
else
    userData = get(gcf, 'UserData');
    if isfield(userData, 'InterpolationMult')
        interpolationMult = userData.InterpolationMult;
    else
        interpolationMult = 1;
    end
    r_area = polyarea(r.XData, r.YData) / interpolationMult^2;
    mid_x = (min(r.XData) + max(r.XData))/2;
    max_y = max(r.YData);
    r.UserData.TextObj = text(mid_x, max_y + 40, sprintf('%3.1f', r_area), 'Color', r.Color, 'FontSize', 12, 'FontWeight', 'bold');
    r.UserData.TextObj.Position(1) = r.UserData.TextObj.Position(1) - r.UserData.TextObj.Extent(3) / 2;
    nested_PrintDataOnAllMarkedAreas(r.Parent);
end

function continue_moving(src,eventdata,r)
%Note: src is now the figure handle, not the axes, so we need to use gca.
coords=get(gca,'currentpoint'); %this updates every time i move the mouse
x=coords(1,1,1);
y=coords(1,2,1);
%get the line's existing coordinates and append the new ones.
r_userData = get(r, 'userdata');
dx = x - r_userData.X;
dy = y - r_userData.Y;
r.XData = r.XData + dx;
r.YData = r.YData + dy;
if isfield(r.UserData, 'TextObj')
    r.UserData.TextObj.Position = r.UserData.TextObj.Position + [dx, dy, 0];
end
r.UserData.X = x;
r.UserData.Y = y;

function done_moving(src,evendata,r)
%all this funciton does is turn the motion function off 
set(gcf,'windowbuttonmotionfcn','')
set(gcf,'windowbuttonupfcn','')

function nested_PrintDataOnAllMarkedAreas(ax)
axChildren = ax.Children;
axChildrenLines = axChildren(strcmp(axChildren.get('type'), 'line'));
if isempty(axChildrenLines)
    return;
end
linesColors = arrayfun(@(line) line.Color, axChildrenLines, 'UniformOutput', false);
yellowLinesIndices = cellfun(@(lineColor) all(lineColor == [1, 1, 0]), linesColors);
redLinesIndices = cellfun(@(lineColor) all(lineColor == [1, 0, 0]), linesColors);

userData = ax.Parent.UserData;
if isfield(userData, 'InterpolationMult')
    interpolationMult = userData.InterpolationMult;
else
    interpolationMult = 1;
end
r_area = arrayfun(@(line) polyarea(line.XData, line.YData) / interpolationMult^2, axChildrenLines);

if isfield(userData, 'OriginalDims')
    originalDims = userData.OriginalDims;
    lineColIndex = arrayfun(@(line) min(ceil(line.XData / originalDims(2))), axChildrenLines); % min instead of unique - to return only one index for each line
    lineRowIndex = arrayfun(@(line) min(ceil(line.YData / originalDims(1))), axChildrenLines); % min instead of unique - to return only one index for each line
    
    imagesInAx = ax.Children(arrayfun(@(child) strcmp(child.Type, 'image'), ax.Children));
    if numel(imagesInAx) == 1
        imageWithAlphaData = imagesInAx;
    else
        itemsInAlphasMaps = arrayfun(@(img) numel(img.AlphaData), imagesInAx);
        imageWithAlphaData = imagesInAx(itemsInAlphasMaps > 1);
    end
        
    b = imageWithAlphaData.AlphaData;
    if numel(b) == 1
        numberOfColumns = 1;
    else
        colsSeparators = sum(b, 1) < 0.5 * size(b, 1); % 50% - because if there is less slices, there will be no frame for missing slices
        numberOfColumns = numel(find(diff(find(colsSeparators))>1));
    end
    slicesNumbers = (lineRowIndex-1) * numberOfColumns + lineColIndex;
    
    [slicesNumbersOrdered, slicesNumbersOrder] = sort(slicesNumbers);
end
fprintf('----------------------------\n');
fprintf('Date of figure-creation and case name: %s\n', strrep(userData.MatFilename, '.mat', ''));
fprintf('Total area of yellow, %3.1f\n', sum(r_area(yellowLinesIndices)));
if isfield(userData, 'OriginalDims')
    for lineIndex = 1:numel(axChildrenLines)
        if yellowLinesIndices(slicesNumbersOrder(lineIndex))
            fprintf('\tSlice #%d, Area, %3.1f\n', slicesNumbersOrdered(lineIndex), r_area(slicesNumbersOrder(lineIndex)));
        end
    end
end
fprintf('Total area of red, %3.1f\n', sum(r_area(redLinesIndices)));
if isfield(userData, 'OriginalDims')
    for lineIndex = 1:numel(axChildrenLines)
        if redLinesIndices(slicesNumbersOrder(lineIndex))
            fprintf('\tSlice #%d, Area, %3.1f\n', slicesNumbersOrdered(lineIndex), r_area(slicesNumbersOrder(lineIndex)));
        end
    end
end

xData = arrayfun(@(line) line.XData, axChildrenLines, 'UniformOutput', false);
yData = arrayfun(@(line) line.YData, axChildrenLines, 'UniformOutput', false);

linesDataToSave = struct('Lines_X', xData, 'Lines_Y', yData, 'LinesColors', linesColors);
dataToSave = struct('UserData', userData, 'LinesData', linesDataToSave);
if isfield(userData, 'MatFilename') && ~isempty(dataToSave.LinesData)
    save(userData.MatFilename, '-struct', 'dataToSave')
end

