function [firstBulbColumn, lastBulbColumn] = FindBulbColumns(firstBulbSliceImage, leftEyeCenters, rightEyeCenters)
%% Find relevant area by eyes centers
eyesMinY = floor(min([[leftEyeCenters.CenterY] - [leftEyeCenters.Radius], [rightEyeCenters.CenterY] - [rightEyeCenters.Radius]]));
eyesMaxY = ceil(max([[leftEyeCenters.CenterY] + [leftEyeCenters.Radius], [rightEyeCenters.CenterY] + [rightEyeCenters.Radius]]));

leftEyeCloseToRightEyeX = round(mean([leftEyeCenters.CenterX] - [leftEyeCenters.Radius]));
rightEyeCloseToLeftEyeX = round(mean([rightEyeCenters.CenterX] + [rightEyeCenters.Radius]));

relevantAreaInImage = firstBulbSliceImage(eyesMinY:eyesMaxY, rightEyeCloseToLeftEyeX:leftEyeCloseToRightEyeX);

%% Find first and last relevant column in the relevant area
countBlanksPerColumn = sum(relevantAreaInImage == 0) / size(relevantAreaInImage, 1);
firstMoreThenHalfBlank = find(countBlanksPerColumn > 0.5, 1);
lastMoreThenHalfBlank = find(countBlanksPerColumn > 0.5, 1, 'last');

%%
if isempty(firstMoreThenHalfBlank)
    firstBulbColumn = rightEyeCloseToLeftEyeX;
    lastBulbColumn = leftEyeCloseToRightEyeX;
else
    firstBulbColumn = rightEyeCloseToLeftEyeX + firstMoreThenHalfBlank - 1;
    lastBulbColumn = rightEyeCloseToLeftEyeX + lastMoreThenHalfBlank - 1;
end