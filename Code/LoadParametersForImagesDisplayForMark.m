function parametersToUse = LoadParametersForImagesDisplayForMark()

parametersToUse = struct();
parametersToUse.('FirstBulbSliceOffsetInMilimeters') = 3; % range: 2-20
parametersToUse.('SlicesDepthToDisplayInMilimeters') = 14;
parametersToUse.('WidthToDisplayInMilimeters') = 22;
parametersToUse.('HeightToDisplayInMilimeters') = 16;
parametersToUse.('InterpolationMult') = 32;
parametersToUse.('RequiredRatioBetweenWidthAndHeight') = 1.4;
