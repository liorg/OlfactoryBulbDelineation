function UpdateWaitbar(waitbarHandle, newValue, newString)
if isempty(waitbarHandle)
    fprintf('[Waitbar] %s\n', newString);
    return;
end

waitbar(newValue, waitbarHandle, newString);
end