function value = GetValue(structObj, fieldname, defaultValue)

if isstruct(structObj) && isfield(structObj, fieldname)
    value = structObj.(fieldname);
else
    value = defaultValue;
end
