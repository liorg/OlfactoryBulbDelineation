function [leftEyeCenters, rightEyeCenters] = SplitCentersBetweenEyes(foundCirclesRelevant)
overlapThreshold = 0.7; % 70%

%% Sort by ImageIndex
[~,sortingOrder] = sort([foundCirclesRelevant.ImageIndex]);
foundCirclesRelevant = foundCirclesRelevant(sortingOrder);
imagesIndices = [foundCirclesRelevant.ImageIndex];

%% Check overlapping matrix, allow one-slice gap
max_x_to_use = ceil(max([foundCirclesRelevant.CenterX] + [foundCirclesRelevant.Radius]));
max_y_to_use = ceil(max([foundCirclesRelevant.CenterY] + [foundCirclesRelevant.Radius]));
[yGrid, xGrid] = ndgrid(1:max_y_to_use, 1:max_x_to_use);
numOfCircles = numel(foundCirclesRelevant);
circlesImages = cell(numOfCircles, 1);
circlesAreas = zeros(numOfCircles, 1);
overlapMatrix = zeros(numOfCircles);
for circleIndex = 1:numOfCircles
    currentCircle = foundCirclesRelevant(circleIndex);
    imagesIndicesDiffFromCurrent = imagesIndices - currentCircle.ImageIndex;
    circlesIndicesFromRelevantSlices = find(imagesIndicesDiffFromCurrent > 0 & imagesIndicesDiffFromCurrent <= 2);
    
    for optionalCircleIndex = circlesIndicesFromRelevantSlices
        optionalCircle = foundCirclesRelevant(optionalCircleIndex);
        
        squaredDistanceBetweenCenters = (optionalCircle.CenterX - currentCircle.CenterX)^2 + (optionalCircle.CenterY - currentCircle.CenterY)^2;
        if squaredDistanceBetweenCenters > max(optionalCircle.Radius, currentCircle.Radius)^2
            continue; % there is no overlapping
        end
        
        if isempty(circlesImages{circleIndex})
            circlesImages{circleIndex} = (xGrid - currentCircle.CenterX).^2 + (yGrid - currentCircle.CenterY).^2 <= currentCircle.Radius^2;
            circlesAreas(circleIndex) = sum(circlesImages{circleIndex}(:));
        end
        
        if isempty(circlesImages{optionalCircleIndex})
            circlesImages{optionalCircleIndex} = (xGrid - optionalCircle.CenterX).^2 + (yGrid - optionalCircle.CenterY).^2 <= optionalCircle.Radius^2;
            circlesAreas(optionalCircleIndex) = sum(circlesImages{optionalCircleIndex}(:));
        end
        
        overlappingImage = circlesImages{circleIndex} .* circlesImages{optionalCircleIndex};
        overlappingArea = sum(overlappingImage(:));
        smallestArea = min(circlesAreas([circleIndex, optionalCircleIndex]));
        
        overlappingPercent = overlappingArea / smallestArea;
        if overlappingPercent > overlapThreshold
            overlapMatrix(circleIndex, optionalCircleIndex) = 1;
            overlapMatrix(optionalCircleIndex, circleIndex) = 1;
        end
    end
end

%% Split to two eyes
overlapBins = findOverlapBins(overlapMatrix);
binsValues = 1:max(overlapBins);
countPerBin = hist(overlapBins, binsValues);

[~, binsForSorted] = sort(countPerBin, 'descend');

if numel(binsForSorted) < 2
    error('Failed to split circles between eyes!');
end

eye1Centers = foundCirclesRelevant(overlapBins == binsForSorted(1));
eye2Centers = foundCirclesRelevant(overlapBins == binsForSorted(2));

%% Find which one is the left and which is the right
eye1AverageX = mean([eye1Centers.CenterX]);
eye2AverageX = mean([eye2Centers.CenterX]);

if eye1AverageX < eye2AverageX
    rightEyeCenters = eye1Centers;
    leftEyeCenters = eye2Centers;
else
    leftEyeCenters = eye1Centers;
    rightEyeCenters = eye2Centers;
end
end

function overlapBins = findOverlapBins(overlapMatrix)
% try
overlapGraph = graph(overlapMatrix);
overlapBins = conncomp(overlapGraph);
% catch % graph is presented only on version 2015b
%     overlapMatrixWithBins = -overlapMatrix;
%     while any (overlapMatrixWithBins < 0)
%         [connectedShapedFirstIndex, connectedShapedSecondIndex] = find(overlapMatrixWithBins < 0, 1, 'first');
%         valuesForConnectedShapes
%     end
% end
    
end