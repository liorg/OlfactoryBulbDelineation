function allStoredCircles = FindCirclesWithoutInsideEdges(images, ...
    circlesRadiiRange, precentOfAllowedEdgesInsideCircle, ...
    precentOfMinimalPerimeterPixels, ...
    waitbarHandle)

maxValue = max(images(:));
numOfImages = size(images, 3);
[imgMapX, imgMapY] = ndgrid(1:size(images,1), 1:size(images, 2));
storedCircles = repmat(struct('ImageIndex', [], 'Centers', [], 'Radii', []), [1, numOfImages]);
for imgIndex = 1:numOfImages
    currentImage = images(:,:,imgIndex);
    imgEdge = edge(currentImage);
    UpdateWaitbar(waitbarHandle, 0.3+0.2*imgIndex/numOfImages, ['Finding circles in slice #' num2str(imgIndex) ' out of ' num2str(numOfImages) ' slices...']);
    [centers, radii] = imfindcircles(currentImage, circlesRadiiRange, 'method', 'TwoStage', ...
        'Sensitivity', sqrt(0.75));
    toRemoveCircles = false(size(radii));
    %% Check for each center if it's on the required areas inside and on the perimeter
    for centerIndex = 1:numel(radii)
        currentCenterDistanceFromMap = (imgMapX - centers(centerIndex, 2)).^2  + (imgMapY - centers(centerIndex, 1)).^2;
        currentPerimeterCirclePixels = abs(currentCenterDistanceFromMap - (radii(centerIndex))^2 - 1) < 2*radii(centerIndex);
        currentInsideCirclePixels = currentCenterDistanceFromMap < (radii(centerIndex)-1).^2;
        currentImage(currentInsideCirclePixels) = maxValue;
        
        circleInsideArea = sum(currentInsideCirclePixels(:));
        edgesInsideCircle = sum(imgEdge(currentInsideCirclePixels));
        edgesPercentInsideCircle = edgesInsideCircle / circleInsideArea * 100;
        
        perimeterArea = sum(currentPerimeterCirclePixels(:));
        edgesInPerimeter = sum(imgEdge(currentPerimeterCirclePixels));
        edgesPercentInsidePerimeter = edgesInPerimeter / perimeterArea * 100;
        
        if edgesPercentInsideCircle > precentOfAllowedEdgesInsideCircle
            toRemoveCircles(centerIndex) = true;
        end
        
        if edgesPercentInsidePerimeter < precentOfMinimalPerimeterPixels
            toRemoveCircles(centerIndex) = true;
        end
    end
    
    %% Create array for all passed circles
    passedCenters = centers(~toRemoveCircles, :);
    passedRadii = radii(~toRemoveCircles);
    
    storedCircles(imgIndex) = struct('ImageIndex', repmat(imgIndex, numel(passedRadii), 1), ...
        'Centers', passedCenters, 'Radii', passedRadii);
end

allStoredImagesIndices = cat(1, storedCircles.ImageIndex);
allStoredCenters = cat(1, storedCircles.Centers);
allStoredRadii = cat(1, storedCircles.Radii);
allStoredCirclesInMatrix = [allStoredImagesIndices, allStoredCenters, allStoredRadii];
allStoredCirclesInCell = num2cell(allStoredCirclesInMatrix);
allStoredCircles = cell2struct(allStoredCirclesInCell, {'ImageIndex', 'CenterX', 'CenterY', 'Radius'}, 2);

end