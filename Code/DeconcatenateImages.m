function [images, coordinates] = DeconcatenateImages(concatenatedImages, rowsInConcatenated, numOfImages, concatenatedCoordinates)

% coordinates in the format <x, y, img-index>

if nargin == 3
    concatenatedCoordinates = [];
end

concatenatedImagesSizes = size(concatenatedImages);
numOfDimensions = numel(concatenatedImagesSizes);

colsInConcatenated = ceil(numOfImages / rowsInConcatenated);

switch numOfDimensions
    case {2, 3}
        fourthDimensionsInOutput = 1;
    case 4
        fourthDimensionsInOutput = concatenatedImagesSizes(3);
    otherwise
        error('Not supported number of dimensions: %d', numOfDimensions);
end

imagesSizes = [concatenatedImagesSizes(1) / rowsInConcatenated, ...
    concatenatedImagesSizes(2) / colsInConcatenated];

images = zeros(imagesSizes(1), ...
    imagesSizes(2), ...
    numOfImages, ...
    fourthDimensionsInOutput);

coordinates = zeros(size(concatenatedCoordinates, 1), ...
    size(concatenatedCoordinates, 2) + 1);

for imgIndex = 1:numOfImages
    colIndex = mod(imgIndex-1, colsInConcatenated);
    rowIndex = floor((imgIndex-1) / colsInConcatenated);
    
    currentRows = imagesSizes(1) * rowIndex + (1:imagesSizes(1));
    currentCols = imagesSizes(2) * colIndex + (1:imagesSizes(2));
    relevantCoordinatesBool = floor((concatenatedCoordinates(:,1) - 1)/imagesSizes(2)) == colIndex & ...
        floor((concatenatedCoordinates(:,2) - 1)/imagesSizes(1)) == rowIndex;
    
    if fourthDimensionsInOutput == 1
        images(:,:, imgIndex) = concatenatedImages(currentRows, currentCols);
    else
        images(:,:,:, imgIndex) = concatenatedImages(currentRows, currentCols, :);
    end
    coordinates(relevantCoordinatesBool, 1:2) = concatenatedCoordinates(relevantCoordinatesBool, 1:2);
    coordinates(relevantCoordinatesBool, 1) = coordinates(relevantCoordinatesBool, 1) - imagesSizes(2) * colIndex;
    coordinates(relevantCoordinatesBool, 2) = coordinates(relevantCoordinatesBool, 2) - imagesSizes(1) * rowIndex;
    coordinates(relevantCoordinatesBool, 3) = imgIndex;
end

end