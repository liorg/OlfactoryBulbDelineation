function PrintToLog(str, varargin)

dateString = datestr(now, 'dd-mmm-yyyy HH:MM:SS.FFF');
fullStr = [dateString ' - ' str];
fprintf(fullStr, varargin{:});
end