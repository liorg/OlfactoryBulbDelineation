function dlg = DialogWithPrintToLog(dialogType, dialogString, dialogName, createMode)

switch dialogType
    case 'Error'
        dialogFunction = @(varargin) errordlg(varargin{:});
    case 'Warning'
        dialogFunction = @(varargin) warndlg(varargin{:});
    case 'Message'
        dialogFunction = @(varargin) msgbox(varargin{:});
    otherwise
        error('Unknown dialog type: %s', dialogType);
end


dialogNameInLog = '';
if nargin == 3 && ~isempty(dialogName)
    dialogNameInLog = [' (named "' dialogName '")'];
end
if iscell(dialogString)
    dialogStringToLog = sprintf('%s\n', dialogString{:});
else
    dialogStringToLog = [dialogString '\n'];
end
Utils.PrintToLog([dialogType ' dialog' dialogNameInLog ' with the following message: ' dialogStringToLog]);

if nargin == 2
    dlg = dialogFunction(dialogString);
elseif nargin == 3
    dlg = dialogFunction(dialogString, dialogName);
elseif nargin == 4
    dlg = dialogFunction(dialogString, dialogName, createMode);
end
end