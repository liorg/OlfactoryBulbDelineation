function imwriteWithCreateFolder(im, pathToWrite)
[folderToWriteTo, ~] = fileparts(pathToWrite);
if ~isempty(folderToWriteTo)
    if isempty(strfind(folderToWriteTo, ':')) % not contain the drive letter
        folderToWriteTo = [cd filesep folderToWriteTo];
    end
    if ~exist(folderToWriteTo, 'dir')
        mkdir(folderToWriteTo);
    end
end
dataFieldName = 'cdata';
if (isstruct(im) && isfield(im, dataFieldName))
    imwrite(im.(dataFieldName), pathToWrite);    
else
    imwrite(im, pathToWrite);
end
end