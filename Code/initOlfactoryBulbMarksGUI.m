function initOlfactoryBulbMarksGUI()
if ~isdeployed
    close all
    clc;

    basedir = cd;
    addpath(genpath(basedir));
    
    % Close the waitbar handles
    isShowingHiddenHandles = get(0, 'ShowHiddenHandles');
    set(0, 'ShowHiddenHandles', 'on');
    waitbarHandles = findobj('Tag', 'TMWWaitbar');
    if ~isempty(waitbarHandles)
        close(waitbarHandles);
    end
    set(0, 'ShowHiddenHandles', isShowingHiddenHandles);
end

OlfactoryBulbMarksGUIManager.getInstance.Initiate();
end



