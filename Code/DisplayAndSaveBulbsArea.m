function DisplayAndSaveBulbsArea(dicImgSortedClear, firstSlice, lastSlice, ...
    minX_ToUse, maxX_ToUse, minY_ToUse, maxY_ToUse, ...
    requiredRatioBetweenWidthAndHeight, ...
    titleStr)

relevantRows = minY_ToUse : maxY_ToUse;
relevantCols = max(1, minX_ToUse):min(maxX_ToUse, size(dicImgSortedClear, 2));
relevantSlices = max(1, firstSlice):min(lastSlice, size(dicImgSortedClear, 3));
dicImagesDims = numel(size(dicImgSortedClear));
switch dicImagesDims
    case 2
        if numel(relevantSlices) > 1 || relevantSlices(1) ~= 1
            error('Not handled 2-dimensions with slice other than 1');
        end
        relvantImages = dicImgSortedClear(relevantRows, relevantCols);
    case 3
        relvantImages = dicImgSortedClear(relevantRows, relevantCols, relevantSlices);
    case 4
        relvantImages = dicImgSortedClear(relevantRows, relevantCols, :, relevantSlices);
    otherwise
        error('Not-handled number of dimensions: %d', dicImagesDims);
end
    
numberOfSlices = lastSlice - firstSlice + 1;
rowsInConcatenated = floor(sqrt(numberOfSlices / requiredRatioBetweenWidthAndHeight * size(relvantImages,2) / size(relvantImages, 1)));
if rowsInConcatenated == 0
    rowsInConcatenated = 1;
end
minmaxValues = [min(relvantImages(:)), max(relvantImages(:))];
relvantImages(:,:,size(relvantImages,3)+1:numberOfSlices) = minmaxValues(1);
relvantImagesConcatenated = ConcatenateImages(relvantImages, rowsInConcatenated);

%% Mark images with blue line
switch dicImagesDims
    case {2, 3}
        relvantImagesMarkerSize = size(relvantImages);
    case 4
        relvantImagesMarkerSize = size(relvantImages);
        relvantImagesMarkerSize(3) = relvantImagesMarkerSize(4);
        relvantImagesMarkerSize(4) = [];
    otherwise
        error('Not-handled number of dimensions: %d', dicImagesDims);
end

relvantImagesMarker = zeros(relvantImagesMarkerSize);
relvantImagesMarkerWidth = ceil(max(size(relvantImagesConcatenated)/1000.*[requiredRatioBetweenWidthAndHeight, 1]));
relvantImagesMarker([1:relvantImagesMarkerWidth,end-(relvantImagesMarkerWidth-1):end],:,:) = 1;
relvantImagesMarker(:,[1:relvantImagesMarkerWidth,end-(relvantImagesMarkerWidth-1):end],:) = 1;
relvantImagesMarkerConcatenated = ConcatenateImages(relvantImagesMarker, rowsInConcatenated);

fig = figure;
markColorMatrix = zeros([size(relvantImagesConcatenated), 3]);
markColorMatrix(:,:,3) = 1; % change color to blue
imshow(markColorMatrix, 'InitialMag', 'fit');
hold on;
imshowHandler = imshow(relvantImagesConcatenated, minmaxValues, 'InitialMag', 'fit');
set(imshowHandler, 'AlphaData', ~relvantImagesMarkerConcatenated);
fig.Units = 'normalized';
fig.Position = [0,0,1,1];
title(titleStr);

%% Save image
img = getframe(gcf);
imwriteWithCreateFolder(img, ['Images', filesep, titleStr, '.bmp']);
end